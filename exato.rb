# encoding: UTF-8

require 'sinatra'
require 'savon'

enable :sessions

before do
  @@client ||= Savon.client("http://paleoprospec.pucrs.br:16002/Exato/Session?wsdl")
  @@key ||= nil
  unless @@key
    response = @@client.request :generate_session
    key = response[:generate_session_response][:return]
    @@key = key
  end
end

get '/' do 
  redirect to '/corpus'
end

get '/corpus' do
  response = @@client.request :get_corpus
  @ret = response[:get_corpus_response][:return]
  erb :index
end

post '/execution' do
  response = @@client.request :set_execution do
    soap.body = {
      session: @@key,
      corpus: params[:corpora],
      filters: [
        '#char_encoding_format isolatin1 \n',
        '#corpus_format tiger_xml \n',
        '#verbose_mode  average \n',
        '#heuristics \n',
        'remove_conjunctions_at_the_beginning yes \n',
        'remove_prepositions_at_the_beginning yes \n',
        'remove_conjunctions_at_the_end yes \n',
        'remove_prepositions_at_the_end yes \n',
        'refuse_NP_with_only_single_words yes \n',
        'remove_articles_at_the_beginning yes \n',
        'remove_all_articles  yes \n',
        'remove_pronouns_at_the_beginning yes \n',
        'remove_all_pronouns  yes \n',
        'refuse_NP_with_numerals  yes \n',
        'refuse_NP_with_invalid_symbols yes \n',
        'refuse_NP_with_invalid_tag yes \n',
        'refuse_NP_starting_with_adverb yes \n',
        'include_implicit_adjectives  yes \n',
        'include_implicit_occurrences yes \n',
        'include_implicit_terms yes \n',
        'refuse_NP_with_nouns no \n',
        'refuse_NP_with_proper_names  no \n',
        'refuse_NP_with_adjectives  no \n',
        'refuse_NP_with_participe_pasts no \n',
        'refuse_NP_that_are_subject no \n',
        'refuse_NP_that_are_object  no \n',
        'refuse_NP_that_are_complement  no \n',
        '#ngrams \n',
        '1grams yes \n',
        '2grams yes \n',
        '3grams yes \n',
        '4grams yes \n',
        '5grams yes \n',
        '6grams yes \n',
        '7grams yes \n',
        '8grams yes \n',
        '9grams yes \n',
        'Mgrams yes \n'
      ]
    }
  end

  if response[:set_execution_response][:return].to_s == 'EXCUTION_SCHEDULED'
    loop do
      response = @@client.request :verify_pending_execution do
        soap.body = {
          session: @@key
        }
      end
      break if response[:verify_pending_execution_response][:return].to_s == 'false'
      sleep 3
    end
    erb :search
  else
  end
end

post '/search' do
  response = @@client.request :search_term_frequency do
    soap.body = {
      session: @@key,
      term: params[:search].to_s
    }
  end

  redirect response[:search_term_frequency_response][:return].to_s if response[:search_term_frequency_response][:return]
  "Desculpe, o Exato não conseguiu retornar a URL da busca do termo. Por favor tente novamente mais tarde."
end

